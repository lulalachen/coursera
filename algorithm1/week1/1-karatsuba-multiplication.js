const multi = (rawNum1, rawNum2) => {
  if (String(rawNum1).length < 2 || String(rawNum2).length < 2) {
    return rawNum1 * rawNum2
  }
  const diff = Math.abs(String(rawNum1).length - String(rawNum2).length)
  const [num1, num2] = padZero(rawNum1, rawNum2)

  const n = Math.floor(String(num1).length / 2) * 2
  const [a, b] = splitNum(num1)
  const [c, d] = splitNum(num2)
  const acResult = multi(a, c)
  const bdResult = multi(b, d)
  const crossResult = multi(a + b, c + d) - acResult - bdResult
  return (
    (Math.pow(10, n) * acResult +
      Math.pow(10, Math.floor(n / 2)) * crossResult +
      bdResult) /
    Math.pow(10, diff)
  )
}

const splitNum = num => {
  const numStr = String(num)
  const length = numStr.length
  return [
    Number(numStr.slice(0, Math.ceil(length / 2))),
    Number(numStr.slice(Math.ceil(length / 2))),
  ]
}

const padZero = (num1, num2) => {
  const diff = Math.abs(String(num1).length - String(num2).length)
  if (String(num1).length > String(num2).length) {
    return [num1, num2 * Math.pow(10, diff)]
  } else if (String(num1).length < String(num2).length) {
    return [num1 * Math.pow(10, diff), num2]
  } else {
    return [num1, num2]
  }
}
