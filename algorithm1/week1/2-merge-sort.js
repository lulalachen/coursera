const merge = (left, right) => {
  let i = 0
  let j = 0
  const result = []

  for (let index = 0; index < left.length + right.length; index++) {
    if (left[i] < right[j] || j >= right.length) {
      result.push(left[i])
      i++
      continue
    }
    if (left[i] >= right[j] || i >= left.length) {
      result.push(right[j])
      j++
      continue
    }
  }

  return result
}

const sort = arr => {
  if (arr.length < 2) return arr

  const left = arr.slice(0, Math.floor(arr.length / 2))
  const right = arr.slice(Math.floor(arr.length / 2))

  return merge(sort(left), sort(right))
}

// sort([2, 3, 5, 9, 12, 42]) //?
sort([5, 3, 2, 42, 3, 12, 9]) //?
// sort([5, 3]) //?
